Das Projekt ist nach: https://gitlab.opencode.de/ig-bvc/policy-entwicklung umgezogen

# Richtlinien zur automatisierten Auditierung ausgewählter BSI-Anforderungen

Eine Reihe von Richtlinien welche Kubernetes-Cluster und Manifeste mit einer Auswahl von Aspekten des BSI SYS1.6 - Container und APP.4.4 - Kubernetes kompatibel macht.

## Anleitung für Mitwirkende
Im Folgenden wird grob der Prozess zur Mitwirkung an den Policies beschrieben. Erstellt bei Unklarheiten gern einen Issue im Projekt.

Vielen Dank für das Interesse an der Mitarbeit! Besonders bei "Nischenthemen" freuen wir uns immer über Hilfe von fachkundigen Mitstreitern.

### Erstellung von Policies

Alle durch die IG BvC als "per Policy prüfbar" identifizierten Anforderungen aus den Ausarbeitungen der IG Betrieb von Containern zu den BSI-Bausteinen [SYS.1.6 Container](https://wikijs.opencode.de/igbvc-sys-1-6.pdf) und [APP.4.4 Kubernetes](https://wikijs.opencode.de/igbvc-app-4-4.pdf) wurden in diesem Repository als entsprechender Issue angelegt.

Es ist geplant, jeden dieser Issues in einem separatem Branch (mit Quelle "dev") zu bearbeiten, indem zunächst zunächst die entsprechende Anforderung aus dem Maßnahmenkatalog der IG BvC dahingehend überprüft wird, welche konkreten Maßnahmen (z.B. Plattformbetreiber muss ...) mit einer oder mehreren automatisierten Prüfungen umsetzbar sind. Wird eine Maßnahme identifiziert, so ist die technische Umsetzung in einer "Policy-Beschreibung" (für Beispiele siehe Markdown-Dokumente unter "/policies/") möglichst Tool-neutral festzuhalten. Sofern möglich hilft auch das Schreiben eines Testfalls bei der späteren Entwicklung der Policies mit den Tools, von daher sind Ideen hier auch willkommen (siehe auch die Testfälle unter "/policies"). 

Die Dateien sollten pro Policy im Ordner "/policies/" in einem Unterordner 000-name-der-policy festgehalten werden. 

Ist die Arbeit an einer Anforderung beendet, sollte dies über das Eröffnen eines Merge-Requests (in den "dev"-Branch) signalisiert werden. Dann kann im Regeltermin des AP4.1 abschließend über die Policy / Policies diskutiert, und eine ID zugewiesen werden.

Standardmäßig wurden nur die Issues erstellt, sofern nicht anders im Issue vermerkt (über Kommentare oder Assignee) müsste der oben beschriebene Prozess von Beginn an durchlaufen werden. Sollte Interesse an der Mitarbeit bestehen: Bitte einfach den eigenen Account als Assignee zuweisen, um weiteren Mitwirkenden zu signalisieren, dass an der Anforderung gearbeitet wird.


### Definition of Done

Ein Issue ist "Done" und kann geschlossen werden, wenn alle Maßnahmen auf die Prüfung mit einer Policy untersucht wurden und dies im Richtlinien-Regeltermin überprüft wurde. Zu allen identifizierten Policies muss ein Markdown-Dokument existieren, welches die Wirkung der Policy beschreibt. Zusätzlich ist in der Regel mindestens ein Positiv- sowie Negativ-Testfall zu entwickeln.  

### Meldung von Bugs / Fehlern

... folgt ...

# Dokumentation zum PoC

...folgt...

## Cross-Compatibility:

in Arbeit

| header | VMware | Rancher | OpenShift |
| ------ | ------ | ------ | ------ |
| kyverno | ja | ? | ? |
| NeuVector | ? | ja | [IBM NeuVector auf OpenShift](https://www.ibm.com/cloud/blog/protecting-kubernetes-and-openshift-workloads-with-neuvector) |
| StackRoxx | ? | ? | ? |


## Kontakt

Bei Fragen / Anregungen / sonstigen Angelegenheiten, die nicht zum Format eines Issues im Projekt passen, meldet euch gern unter t.missal@dvz-mv.de oder f.friese@dvz-mv.de
