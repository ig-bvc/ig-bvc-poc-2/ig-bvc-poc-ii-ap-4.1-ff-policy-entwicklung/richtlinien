# Policy ID: 026

## BSI-Anforderungen

### SYS.1.6.A21
*Plattformbetreiber* **MUSS** ​die Isolation durch die minimal erforderlichen Berechtigungen auf Ressourcen und Kernelfunktionen sicherstellen und Verstöße gegen die Richtlinien unterbinden und protokollieren.

*Plattformbetreiber* **MUSS** Richtlinien für Softwarebetreiber zur Einschränkung von Zugriffen und Berechtigungen auf Ressourcen vorgeben.

*Softwarebetreiber* **MUSS** Richtlinien zur Einschränkung von Zugriffen und Berechtigungen auf Ressourcen definieren.


## Formale Beschreibung

Sofern verfügbar, sind Container mit den Runtime-Default-Profilen für Apparmor / SELinux zu starten.

## Technische Umsetzungsvorschläge (optional)

- Prüfung auf annotation container.apparmor.security.beta.kubernetes.io/<container_name>: runtime/default

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Link // Standardmäßig implementiert (mit Link)
