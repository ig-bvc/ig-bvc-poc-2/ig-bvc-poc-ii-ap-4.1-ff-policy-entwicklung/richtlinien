# Policy ID: 019

## BSI-Anforderungen

### APP.4.4.A9
*Softwarebetreiber* **SOLLTE** ​Anforderungen des Softwarelieferanten zur Nutzung des default-Service-Accounts ablehnen. 

## Formale Beschreibung

Die Nutzung des "default"-Service-Accounts eines Namespaces sollte unterbunden werden.

## Technische Umsetzungsvorschläge (optional)

- AdmissionControl in Pod: Prüfung auf "serviceAccountName"

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Link // Standardmäßig implementiert (mit Link)
