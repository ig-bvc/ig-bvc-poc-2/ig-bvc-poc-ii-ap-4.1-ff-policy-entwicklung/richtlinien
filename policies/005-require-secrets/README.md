# Policy ID: 005

## BSI-Anforderungen

### SYS.1.6.A8
*Softwarebetreiber* **MUSS** Zugangsdaten (z.B. Passworte, geheime/private Schlüssel, API-Keys, Schlüssel für symmetrische Verschlüsselungen) in geschützten Bereichen hinterlegen. Dafür sind Kubernetes Secrets oder gleichwertige Systeme zu benutzen.


## Formale Beschreibung

Die Nutzung von Environment-Variablen, dessen Keys Zeichenketten enthalten, welche auf geheime Daten hinweisen, sollte nur erlaubt sein, sofern diesen ein Secret zugeordnet ist. Das Einsetzen von Passwörtern im Klartext (z.B. über ConfigMaps) sollte unterbunden werden.

## Technische Umsetzungsvorschläge (optional)

- Admission Control: wenn in Pod unter .spec.containers[].env[].name Zeichenketten mit dem Inhalt "secret", "key", "password" / "passwort" / "kennwort", "token", "user" / "nutzer", ..., auftauchen, kontrolliere, ob dem "name" ein .valueFrom.secretKeyRef zugeordnet ist. Wenn nicht, blocke die Anfrage. Siehe Doku https://kubernetes.io/docs/concepts/configuration/secret/#using-secrets-as-environment-variables

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Link // Standardmäßig implementiert (mit Link)

