# Policy ID: 027

## BSI-Anforderungen

### SYS.1.6.A21
*Plattformbetreiber* **MUSS** ​die Isolation durch die minimal erforderlichen Berechtigungen auf Ressourcen und Kernelfunktionen sicherstellen und Verstöße gegen die Richtlinien unterbinden und protokollieren.

*Plattformbetreiber* **MUSS** Richtlinien für Softwarebetreiber zur Einschränkung von Zugriffen und Berechtigungen auf Ressourcen vorgeben.

*Softwarebetreiber* **MUSS** Richtlinien zur Einschränkung von Zugriffen und Berechtigungen auf Ressourcen definieren.


## Formale Beschreibung

Für die Ausführung von Containern sollte ein möglichst restriktives Seccomp-Profil angewendet werden.

## Technische Umsetzungsvorschläge (optional)

- "möglichst restriktiv" sollte hier nicht unbedingt heißen, dass für jede Anwendung eigene Profile geschrieben werden. Denkbar ist auch die Bereitstellung von Profilen mit verschiedenem Sicherheitslevel durch den PLattformbetreiber.
- Prüfung auf securityContext.seccompProfile

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Link // Standardmäßig implementiert (mit Link)
