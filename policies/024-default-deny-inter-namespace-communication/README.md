# Policy ID: 024

## BSI-Anforderungen

### APP.4.4.A7
*Softwarebetreiber* **SOLLTE** ​Netzverbindungen zwischen Kubernetes-Namespaces standardmäßig untersagen und nur für den Betrieb der Anwendung benötigte Verbindungen freischalten.

## Formale Beschreibung

Die Fähigkeit eines Pods, Netzwerkverbindungen zu Services in anderen Namespaces aufzubauen, sollte standardmäßig unterbunden und explizit freigeschaltet werden, wenn der Betrieb der Anwendung dies benötigt.

## Technische Umsetzungsvorschläge (optional)

- Prüfen auf NetworkPolicy, welche alle Verbindungen in andere Namespaces untersagt

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Link // Standardmäßig implementiert (mit Link)
