# Policy ID: 011

## BSI-Anforderungen

### SYS.1.6.A19
*Softwarelieferant* **MUSS** ​notwendige persistente Volumen und deren Nutzung (z. B. RWM, RWO, RW/RO) im Deployment beschreiben.

## Formale Beschreibung

Bei Erstellung eines PersistentVolumes muss ein möglichst restriktiver Access-Mode angegeben werden.

## Technische Umsetzungsvorschläge (optional)

- Admission Control: Statische Prüfung auf Vorhandensein von PersistentVolume .spec.accessModes

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Link // Standardmäßig implementiert (mit Link)
