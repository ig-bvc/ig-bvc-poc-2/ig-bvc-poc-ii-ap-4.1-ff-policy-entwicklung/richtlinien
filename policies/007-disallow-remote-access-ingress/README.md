# Policy ID: 007

## BSI-Anforderungen

### SYS.1.6.A16
*Softwarebetreiber* **MUSS** ​sicherstellen, dass administrative Zugriffe auf Applikations-Container immer über die Container-Runtime erfolgen.

## Formale Beschreibung

Es muss sichergestellt werden, dass zu einem Container keine Netzwerkverbindungen über typische Fernwartungsports geöffnet werden können.

## Technische Umsetzungsvorschläge (optional)

- Default-Deny-NetworkPolicy + AdmissionControl: Verbieten des Erstellens vom NetworkPolicy-Objekten, die Ingress Traffic auf z.B. Port 22 freischalten würden..

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Link // Standardmäßig implementiert (mit Link)
