# Policy ID: 004

## BSI-Anforderungen

### SYS.1.6.A6
*Plattformbetreiber* **SOLLTE** ​​automatisierte Policies implementieren, die die Herkunft, Vertrauenswürdigkeit und Integrität der Images prüfen und durchsetzen.

*Plattformbetreiber* **MUSS** dem Softwarebetreiber ein vertrauenswürdiges Repository zur Anlieferung der Images und Build-Spezifikationen bereitstellen.

## Formale Beschreibung

Images sollten nur aus vertrauenswürdigen Registries bezogen werden, von daher ist die Nutzung von Images, welche aus anderen Registries wie z.B. docker.io kommen, zu unterbinden.

## Technische Umsetzungsvorschläge (optional)

- z.B. Admission Control, Prüfung zur Laufzeit...

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Link // Standardmäßig implementiert (mit Link)
