# Policy ID: 002

## BSI-Anforderungen

### SYS.1.6.A15
*Plattformbetreiber* **muss** ​​Quotierungen auf dem Namespace setzen.


## Formale Beschreibung

Eine Policy muss sicherstellen, dass bei Erstellung eines Pods in einem Namespace ein [Resource-Quota](https://kubernetes.io/docs/concepts/policy/resource-quotas/) für diesen Namespace existiert.

## Technische Umsetzungsvorschläge (optional)

- Admission Control: Prüfung auf existierendes Resource-Quota per API-Anfrage

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Link // Standardmäßig implementiert (mit Link)
