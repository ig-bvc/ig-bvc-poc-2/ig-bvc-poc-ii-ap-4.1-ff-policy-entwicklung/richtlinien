# Policy ID: 012

## BSI-Anforderungen

### SYS.1.6.A23
*Softwarelieferant* **MUSS** ​sicherstellen, dass Daten nicht in das Root-File-System geschrieben werden können.  

*Softwarelieferant* **MUSS** ​Daten in eigene Mount-Verzeichnisse schreiben. 

*Softwarelieferant* **MUSS** ermöglichen, dass ​Daten die im Container ausschließlich gelesen werden, als Read-Only Volume in den Container eingebunden werden können.

*Softwarebetreiber* **MUSS** Daten die im Container ausschließlich gelesen werden, als Read-Only Volume in den Container einbinden.

*Softwarebetreiber* **SOLL** sicherstellen, dass Dateisysteme nicht mit Schreibrechten eingebunden werden.


## Formale Beschreibung

Es muss sichergestellt werden, dass Container mit einem unbeschreibbarem Root-Filesystem gestartet werden.


## Technische Umsetzungsvorschläge (optional)

- readOnlyRootFilesystem: true

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Link // Standardmäßig implementiert (mit Link)
