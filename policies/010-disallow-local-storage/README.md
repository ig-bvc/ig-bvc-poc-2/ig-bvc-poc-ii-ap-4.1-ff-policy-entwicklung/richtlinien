# Policy ID: 010

## BSI-Anforderungen

### SYS.1.6.A19
*Softwarelieferant / Softwarebetreiber* **SOLLTE** keine lokalen Speicher der Workernodes benutzen.

## Formale Beschreibung

Die Erstellung von PersistentVolumes des Typs "local" sollte unterbunden werden. 

## Technische Umsetzungsvorschläge (optional)

- Admission Control: Statische Prüfung auf PV .spec.local

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Link // Standardmäßig implementiert (mit Link)
