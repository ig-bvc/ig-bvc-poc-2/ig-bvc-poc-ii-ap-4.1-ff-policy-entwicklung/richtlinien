# Policy ID: 025

## BSI-Anforderungen

### APP.4.4.A21

Bei einem erhöhten Risiko für Fremdeinwirkung und einem sehr hohen Schutzbedarf SOLLTEN Pods regelmäßig gestoppt und neu gestartet werden. Kein Pod SOLLTE länger als 24 Stunden laufen. 

*Softwarebetreiber* **SOLLTE** ​die Software so betreiben, dass ein automatischer Restart der Pods in einem definierten Zeitintervall (<= 24 Stunden) so erfolgen kann, dass Benutzer-Sessions ohne Unterbrechung bestehen bleiben und die Qualität der Verfügbarkeit nicht beinträchtigt wird.


## Formale Beschreibung

Softwarebetreiber sollten darauf hingewiesen werden, dass ihre Pods bereits länger als 24 Stunden laufen. Die Umsetzung der Restarts liegt beim Softwarebetreiber.

## Technische Umsetzungsvorschläge (optional)

- Prüfung auf vergangene Zeit seit Start

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Link // Standardmäßig implementiert (mit Link)
