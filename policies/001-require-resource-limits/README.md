# Policy ID: 001

## BSI-Anforderungen

### SYS.1.6.A15
*Softwarebetreiber* **muss** ​​die angegebenen Begrenzungen der Ressourcen (Limits) durch entsprechende Konfiguration der Container umsetzen.
	

## Formale Beschreibung

Eine Policy muss sicherstellen, dass Ressourcenbegrenzungen ([Requests und Limits](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/)) für alle im Cluster laufenden Container existieren.

## Technische Umsetzungsvorschläge (optional)

- Admission Control (statische Prüfung auf Pod .spec.containers.resources.requests/limits)

## Technische Umsetzungen

- Kyverno: Link // Standardmäßig implementiert (mit Link)
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Link // Standardmäßig implementiert (mit Link)
