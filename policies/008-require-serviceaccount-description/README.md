# Policy ID: 008

## BSI-Anforderungen

### APP.4.4.A3
* *Softwarelieferant* **MUSS** darlegen, für welche Zwecke technische User (z.B. Kubernetes ServiceAccounts) Berechtigungen erfordern.
* *Softwarebetreiber* **MUSS** darlegen, für welche Zwecke technische User (z.B. Kubernetes ServiceAccounts) Berechtigungen erfordern.
* *Plattformbetreiber* **MUSS** dokumentieren, für welche Zwecke technische User (z.B. Kubernetes ServiceAccounts) Berechtigungen erfordern.

## Formale Beschreibung

Die Nutzung einer Annotation `policies.opencode.de/description` sollte für jeden ServiceAccount überprüft werden

## Technische Umsetzungsvorschläge (optional)

- Innerhalb von CI/CD Pipeplines sollten Kubernetes Manifeste auf die Annotation geprüft werden.
- Innerhalb des Clusters sollten Kubernetes Manifeste auf die Annotation geprüft werden.
- Die Policy sollte ausschliesslich warnen, da schon in einem Standard Kubernetes-Cluster eine Vielzahl von Serviceaccounts bereits vorhanden ist, und eine restriktive Policy die Funktionalität des Clusters einschränken kann.

## Technische Umsetzungen

- Kyverno:
- NeuVector:
- StackRox:
