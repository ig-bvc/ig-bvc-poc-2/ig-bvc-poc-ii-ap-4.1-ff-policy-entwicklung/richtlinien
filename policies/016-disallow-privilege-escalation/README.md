# Policy ID: 016

## BSI-Anforderungen

### SYS.1.6.A17
*Plattformbetreiber* **MUSS** ​sicherstellen, dass die Nutzung erweiterter Rechte bei der Ausführung der Container-Runtime und von den instanziierten Containern verhindert wird. Ausnahmen hiervon sind Container, welche Aufgaben des Host-Systems übernehmen.

## Formale Beschreibung

Das Setzen von Set-UID oder Set-GID Bits auf Child-Prozesse eines Containers sollte unterbunden werden. 

## Technische Umsetzungsvorschläge (optional)

- securityContext: allowPrivilegeEscalation=false

## Technische Umsetzungen

- Kyverno: https://gitlab.opencode.de/ig-bvc/ig-bvc-poc-2/ig-bvc-poc-ii-ap-4.1-ff-policy-entwicklung/rl-kyverno/-/raw/master/policies/deny-privilege-escalation.yaml
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Link // Standardmäßig implementiert (mit Link)
