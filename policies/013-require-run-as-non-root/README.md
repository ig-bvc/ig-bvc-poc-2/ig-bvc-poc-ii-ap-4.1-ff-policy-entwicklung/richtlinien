# Policy ID: 013

## BSI-Anforderungen

### SYS.1.6.A17
*Plattformbetreiber* **MUSS** ​sicherstellen, dass die Ausführung der Container-Runtime und von den instanziierten Containern als root User nicht möglich ist. Ausnahmen hiervon sind Container, welche Aufgaben des Host-Systems übernehmen.

## SYS.1.6.A18
*Plattformbetreiber* **MUSS** sicherstellen, dass Container nicht unter Root-Rechten laufen.

## Formale Beschreibung

Die Ausführung von Containern als Root-User (UID 0) ist zu unterbinden.


## Technische Umsetzungsvorschläge (optional)

- Pod securityContext: runAsNonRoot: true && runAsUser > 0

## Technische Umsetzungen

- Kyverno: https://gitlab.opencode.de/ig-bvc/ig-bvc-poc-2/ig-bvc-poc-ii-ap-4.1-ff-policy-entwicklung/rl-kyverno/-/raw/master/policies/require-run-as-non-root.yaml
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Link // Standardmäßig implementiert (mit Link)
