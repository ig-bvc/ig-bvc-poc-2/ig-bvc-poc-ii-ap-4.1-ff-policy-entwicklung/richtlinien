# Policy ID: 003

## BSI-Anforderungen

### SYS.1.6.A6
*Plattformbetreiber* **SOLLTE** ​​automatisierte Policies implementieren, die die Herkunft, Vertrauenswürdigkeit und Integrität der Images prüfen und durchsetzen.


## Formale Beschreibung

Die Nutzung des "latest" Image-Tags sollte unterbunden werden. Stattdessen sollten klare Versionsnummern angegeben werden.

## Technische Umsetzungsvorschläge (optional)

- z.B. Admission Control, Prüfung zur Laufzeit...

## Technische Umsetzungen

- Kyverno: https://gitlab.opencode.de/ig-bvc/ig-bvc-poc-2/ig-bvc-poc-ii-ap-4.1-ff-policy-entwicklung/rl-kyverno/-/raw/master/policies/disallow-latest-tag.yaml
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Link // Standardmäßig implementiert (mit Link)
