# Policy ID: 009

## BSI-Anforderungen

### SYS.1.6.A19
*Softwarelieferant / Softwarebetreiber* **SOLLTE** keine lokalen Speicher der Workernodes benutzen.

## Formale Beschreibung

Die Nutzung von Volumes des Typs "hostPath" sollte unterbunden werden. 

## Technische Umsetzungsvorschläge (optional)

- Admission Control: Statische Prüfung auf Pod .spec.volumes.hostPath

## Technische Umsetzungen

- Kyverno: https://gitlab.opencode.de/ig-bvc/ig-bvc-poc-2/ig-bvc-poc-ii-ap-4.1-ff-policy-entwicklung/rl-kyverno/-/blob/master/policies/disallow-host-path.yaml
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Link // Standardmäßig implementiert (mit Link)
