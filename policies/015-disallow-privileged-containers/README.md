# Policy ID: 015

## BSI-Anforderungen

### SYS.1.6.A17
*Softwarebetreiber* **MUSS** die Konfiguration so umsetzen, dass keine erweiterten Privilegien angefordert werden. Ausnahmen hiervon sind Container, welche Aufgaben des Host-Systems übernehmen.


## Formale Beschreibung

Es ist sicherzustellen, dass Container nicht im privileged-Mode ausgeführt werden können.


## Technische Umsetzungsvorschläge (optional)

- Pod securityContext: privileged=false

## Technische Umsetzungen

- Kyverno: https://gitlab.opencode.de/ig-bvc/ig-bvc-poc-2/ig-bvc-poc-ii-ap-4.1-ff-policy-entwicklung/rl-kyverno/-/raw/master/policies/disallow-privileged-containers.yaml
- NeuVector: Link // Standardmäßig implementiert (mit Link)
- StackRox: Link // Standardmäßig implementiert (mit Link)
